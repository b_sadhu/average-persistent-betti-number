import os
import numpy as np
import subprocess
from shutil import copyfile
import collections
import itertools
import pickle
from statistics import mean 
from scipy.spatial.distance import squareform, pdist
import pandas as pd
import gudhi as gd
import matplotlib.pyplot as plt
'''
WATER Average betti number calculations   
'''

#USER INPUT HERE:  
#SIMULATION PARAMETERS
#in ps unit only

#options: DGA, SOL 
#system = 'SOL'
system = 'DGA'
traj_length = 100000
sampling_dt = 100
tpr = 'nvt.tpr'
topol = 'dt.top'
xtc = 'nvt-nopbc-100ns.xtc'
ndx = 'rdf'

#-skip is used in trjconv. use 1 if all frames of xtc need to convert in gro; else use higher value to skip frames; for 2, every second frame is skipped
write_freq = 10
start_time = 0
end_time = 100000
dir_name = 'Average_PBN'

#directory for files
if os.path.exists('%s' % dir_name) == False:
	os.mkdir('%s' % dir_name)
os.chdir('%s' % dir_name)

# get molecule number and name
num_ext = 0
num_wat = 0
num_ntc = 0
with open('../../%s' % topol) as top:
	for line in top:
		if len(line) > 1:
			if line.split()[0] == 'DGA':
				ext = 'DGA'
				o_atom_num = '1'
				num_ext = int(line.split()[1]) + 1
			if line.split()[0] == 'DHO':
				ext2 = 'DHO'
				num_ext2 = int(line.split()[1])
			if line.split()[0] == 'SOL':
				num_wat = int(line.split()[1])
			if line.split()[0] == 'NTR':
                                num_ntc = int(line.split()[1])
'''
# make ndx file
make_ndx = subprocess.Popen(('gmx_mpi_d','make_ndx','-f','../../%s' % tpr,'-o','%s_cn_prename.ndx' % ndx),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
stdin = make_ndx.communicate(input='r DGA & a O1 O2 O3 N1 N2 C1 C2 C3 C4 C5 C6 C7 C8 C9 C10 C11 C12 C13 C14 C15 C16 C17 C18 C19 C20 C21 C22 C23 C24 C25 C26 C27 C28 C29 C30 C31 C32 C33 C34 C35 C36\nq\n'.encode())[0]

# rename index group
with open('%s_cn_prename.ndx' % ndx) as ndx_auto:
	with open('%s_cn.ndx' % ndx,'w') as ndxfile:
		for line in ndx_auto:
			if '&' in line and 'DGA' in line:
				ndxfile.write('[ CN_ext ]\n')
			elif 'SOL' in line:
				ndxfile.write('[ CN_water ]\n')
			else:
				ndxfile.write(line)
os.remove('%s_cn_prename.ndx' % ndx)

# make .gro traj for ext and water
make_gro = subprocess.Popen(('gmx_mpi_d','trjconv','-f','../../%s' % xtc,'-s','../../%s' % tpr,'-skip','%s' % write_freq,'-b','%s' % start_time,'-e','%s' % end_time,'-n','%s_cn.ndx' % ndx,'-o','%s_cn_.gro' % ext,'-sep'),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
stdin = make_gro.communicate(input='CN_ext\n'.encode())[0]

if num_wat > 0:
	make_gro = subprocess.Popen(('gmx_mpi_d','trjconv','-f','../../%s' % xtc,'-s','../../%s' % tpr,'-skip','%s' % write_freq,'-b','%s' % start_time,'-e','%s' % end_time,'-n','%s_cn.ndx' % ndx,'-o','SOL_cn_.gro','-sep'),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	stdin = make_gro.communicate('CN_water\n'.encode())[0]

if num_ntc > 0:
        make_gro = subprocess.Popen(('gmx_mpi_d','trjconv','-f','../../%s' % xtc,'-s','../../%s' % tpr, '-skip','%s' % write_freq,'-b','%s' % start_time,'-e','%s' % end_time,'-n','%s_cn.ndx' % ndx,'-o','NITRI_cn_.gro','-sep'),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
        stdin = make_gro.communicate('NITRI\n'.encode())[0]

# convert .gro files to .xyz

if num_wat > 0:
	for time in range(0,int(traj_length/sampling_dt)+1,1):
		with open('SOL_cn_%s.gro' % time) as gro:
			with open('SOL_cn_%s.xyz' % str(time),'w') as xyz:
				print('making xyz for time:',time)
				for i, line in enumerate(gro):
					if i ==0:
						pass
					elif i == 1:
						num_atoms = int(line.split()[0])
						xyz.write(str(num_atoms) + '\n')
						xyz.write('\n')
					elif (i > 1) & (i < num_atoms + 2):
						xyz.write(line.split()[1][0]+' ')
						xyz.write(str(round(1000*float(line.split()[-3]))/100)+' ')
						xyz.write(str(round(1000*float(line.split()[-2]))/100)+' ')
						xyz.write(str(round(1000*float(line.split()[-1]))/100)+'\n')

for time in range(0,int(traj_length/sampling_dt)+1,1):
	with open('%s_cn_%s.gro' % (ext,time)) as gro:
		with open('%s_cn_%s.xyz' % (ext,str(time)),'w') as xyz:
			print('making xyz for time:',time)
			for i, line in enumerate(gro):
				if i ==0:
					pass
				elif i == 1:
					num_atoms = int(line.split()[0])
					xyz.write(str(num_atoms) + '\n')
					xyz.write('\n')
				elif (i > 1) & (i < num_atoms + 2):
					xyz.write(line.split()[1][0]+' ')
					xyz.write(str(round(1000*float(line.split()[-3]))/100)+' ')
					xyz.write(str(round(1000*float(line.split()[-2]))/100)+' ')
					xyz.write(str(round(1000*float(line.split()[-1]))/100)+'\n')

if num_ntc > 0:
        for time in range(0,int(traj_length/sampling_dt)+1,1):
                with open('NITRI_cn_%s.gro' % time) as gro:
                        with open('NITRI_cn_%s.xyz' % str(time),'w') as xyz:
                                print('making xyz for time:',time)
                                for i, line in enumerate(gro):
                                        if i ==0:
                                                pass
                                        elif i == 1:
                                                num_atoms = int(line.split()[0])
                                                xyz.write(str(num_atoms) + '\n')
                                                xyz.write('\n')
                                        elif (i > 1) & (i < num_atoms + 2):
                                                xyz.write(line.split()[1][0]+' ')
                                                xyz.write(str(round(1000*float(line.split()[-3]))/100)+' ')
                                                xyz.write(str(round(1000*float(line.split()[-2]))/100)+' ')
                                                xyz.write(str(round(1000*float(line.split()[-1]))/100)+'\n')


'''
for_loop_number = 0
sum_pbe_frame = []
for time in range(0,int(traj_length/sampling_dt)+1,1):
	print('time:',time)
	f = open('../%s/%s_cn_%s.xyz' % (dir_name,system,str(time)), "r")
	g = open("corrected-coord.txt", "w")
	for line in f:
		if line.strip():
			g.write("\t".join(line.split()[1:]) + "\n")
	f.close()
	fl_coords = []
	with open("corrected-coord.txt", "r") as g:
		lines = g.readlines()
		#change to 113 if whole DGA molecule is selected in xyz file, for truncated DGA with 41 atoms, use 41
		#1::3 for water,selects OW only
		if system == 'DGA':
			desired_lines = lines[1::41]
		if system == 'SOL':
			desired_lines = lines[1::3]
		#desired_lines = lines[1::3]
		for line in desired_lines:
			coords = [np.array(line.replace("'", "").rstrip('\n').replace('\t', ' ').split(' '))]
			for arr in coords:
				l = arr.astype(float)
				fl_coords.append(l)

	arr = np.asarray(fl_coords)
	sf1 = squareform(pdist(arr, 'euclidean'))
	dist_list = [pd.DataFrame(sf1)]
	mat_dist0 = dist_list[0]
	skeleton_0 = gd.RipsComplex(distance_matrix=mat_dist0.values, max_edge_length=50.0)
	Rips_simplex_tree_0 = skeleton_0.create_simplex_tree(max_dimension=3)
	Rips_simplex_tree_0.dimension()
	result_str = 'Rips complex is of dimension ' + repr(Rips_simplex_tree_0.dimension()) + ' - ' +     repr(Rips_simplex_tree_0.num_simplices()) + ' simplices - ' +     repr(Rips_simplex_tree_0.num_vertices()) + ' vertices.'

	print(result_str)

	fmt = '%s -> %.2f'

	#for filtered_value in Rips_simplex_tree_0.get_filtration():
	#	print(fmt % tuple(filtered_value))
	diag = Rips_simplex_tree_0.persistence(min_persistence=0)
	
	if time == 1000 or time == 25000 or time == 50000 or time == 75000 or time == 100000:
		gd.plot_persistence_barcode(diag)
		plt.savefig('../%s_pbarcode_%s.png'% (time,system), dpi=600)
		gd.plot_persistence_diagram(diag,legend=True)
		plt.savefig('../%s_pdiagram_%s.png'% (time,system), dpi=600)
	
	######################AVERAGE PERSISTENT BETTI NUMBER###################################
	print('f_loop',for_loop_number)
	#fixing window of plot(euclidean distance)
	i = np.arange(0,51,0.5)
	j = np.arange(0.5,51.5,0.5)
	dim_zip = len(list(zip(i,j)))
	betti_number_list = []
	for i, j in zip(i,j):
		betti_number = Rips_simplex_tree_0.persistent_betti_numbers(i,j)
		betti_number_list.append(betti_number)
	with open('betti-pbn-%s.txt' % for_loop_number,'wb') as wfp:
		pickle.dump(betti_number_list, wfp)
	wfp.close()
	if for_loop_number == 0:
		with open('betti-pbn-%s.txt' % (for_loop_number),'rb') as fp1:
			b_old = pickle.load(fp1)
		for each in b_old:
			sum_b0 = each
			sum_pbe_frame.append(sum_b0)
	if for_loop_number > 0:
		with open('betti-pbn-%s.txt' % (for_loop_number),'rb') as fp1:
			b_new = pickle.load(fp1)
			for i,j in zip((range(len(b_new))), range(len(sum_pbe_frame))):
				i_np = np.array(b_new[i])
				j_np = np.array(sum_pbe_frame[j])
				sumaa = i_np + j_np
				for idx, item in enumerate(sum_pbe_frame):
					if i == idx:
						sum_pbe_frame[idx] = sumaa
	for_loop_number += 1
	print('sum_pbe_frame:',sum_pbe_frame)
print('sum_pbe_frame',sum_pbe_frame)

with open('../betti-sum-pbn-%s.txt' % system,'wb') as wfp:
        pickle.dump(sum_pbe_frame, wfp)
        
print('number of frames', for_loop_number)

os.system('rm *.gro')
os.system('rm betti-pbn-*')
