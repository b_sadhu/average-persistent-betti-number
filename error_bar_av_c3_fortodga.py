import os
global np
import numpy as np
import subprocess
from shutil import copyfile
import collections
import itertools
import pickle
from statistics import mean 
from scipy.spatial.distance import squareform, pdist
import pandas as pd
import gudhi as gd
import matplotlib.pyplot as plt
from my_functions_v10 import *

'''
WATER Average betti number calculations   
'''

#USER INPUT HERE:  
#SIMULATION PARAMETERS
#in ps unit only

dir_name = 'Average_PBN'
num_of_molecules = 60
system = 'DGA'
max_dimension_in_it = 3
max_edge_length_in_it = 72
min_persistence_in_it = 0
dimension_in_it = 1
start_x_axis_in_plot = 1
end_x_axis_in_plot = 70
start_y_axis_in_plot = 1
end_y_axis_in_plot = 70
dimension = 1

os.chdir('%s' % dir_name)
#os.system('rm %s_Heterogeneity_%s.p' % (max_edge_length_in_it,dir_name))

###list of zero in file creation###
def zerolistmaker(n):
	listofzeros = [0] * n
	return listofzeros



sum_pbe_frame = []
for time in range(0,1001,1):
	print('time:',time)
	array = xyz_to_array_of_coordinates("DGA_cn_%s.xyz" % (time))
	mat = mat_from_one_atom_approach(array,num_of_molecules,system)
	mat = mat[0]
	Rips_simplex_tree_0, diag = get_diag_from_distance_matrix(mat,max_dimension_in_it,max_edge_length_in_it,min_persistence_in_it)
	
	######################AVERAGE PERSISTENT BETTI NUMBER###################################
	#fixing window of plot(euclidean distance)
	i = np.arange(0,71,0.5)
	j = np.arange(0.5,71.5,0.5)
	dim_zip = len(list(zip(i,j)))
	betti_number_list = []
	for i, j in zip(i,j):
		betti_number = Rips_simplex_tree_0.persistent_betti_numbers(i,j)
		betti_number_list.append(betti_number)
	sum_pbe_frame.append(betti_number_list)
	print(betti_number_list)

with open('../persistence_betti_list_%s_%s.p' % (dir_name,system),'wb') as wfp:
	pickle.dump(sum_pbe_frame, wfp)
        

