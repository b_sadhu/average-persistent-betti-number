import os
global np
import numpy as np
import subprocess
from shutil import copyfile
import collections
import itertools
import pickle
from statistics import mean 
from scipy.spatial.distance import squareform, pdist
import pandas as pd
import gudhi as gd
import matplotlib.pyplot as plt
from my_functions_v9 import *

'''
WATER Average betti number calculations   
'''

#USER INPUT HERE:  
#SIMULATION PARAMETERS
#in ps unit only

dir_name = 'Av_PBN_0p5M_DBTD_constrained'
num_of_molecules = 301
system = 'TDMA'
max_dimension_in_it = 3
max_edge_length_in_it = 52
min_persistence_in_it = 0
dimension_in_it = 1
start_x_axis_in_plot = 1
end_x_axis_in_plot = 30
start_y_axis_in_plot = 1
end_y_axis_in_plot = 30
dimension = 1

os.chdir('%s' % dir_name)
#os.system('rm %s_Heterogeneity_%s.p' % (max_edge_length_in_it,dir_name))

###list of zero in file creation###
def zerolistmaker(n):
	listofzeros = [0] * n
	return listofzeros



sum_pbe_frame = []
for time in range(0,501,1):
	print('time:',time)
	array = xyz_to_array_of_coordinates("DMDBT_cn_%s.xyz" % (time))
	mat = mat_from_one_atom_approach_malonamide(array,num_of_molecules,system)
	mat = mat[0]
	Rips_simplex_tree_0, diag = get_diag_from_distance_matrix(mat,max_dimension_in_it,max_edge_length_in_it,min_persistence_in_it)
	
	######################AVERAGE PERSISTENT BETTI NUMBER###################################
	#fixing window of plot(euclidean distance)
	i = np.arange(0,51,0.5)
	j = np.arange(0.5,51.5,0.5)
	dim_zip = len(list(zip(i,j)))
	betti_number_list = []
	for i, j in zip(i,j):
		betti_number = Rips_simplex_tree_0.persistent_betti_numbers(i,j)
		betti_number_list.append(betti_number)
	sum_pbe_frame.append(betti_number_list)
	print(betti_number_list)

with open('../persistence_betti_list_%s.p' % (dir_name),'wb') as wfp:
	pickle.dump(sum_pbe_frame, wfp)
        

