#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

np.set_printoptions(suppress=True)

#y-axis
for_loop_number = 1001
dfx = pd.DataFrame(np.arange(0,5,0.05))

with open('betti-sum-pbn-water_b3.txt','rb') as fp1:
    X1 = pickle.load(fp1)
df1 = pd.DataFrame(X1)/1001
df1.columns = ['B0', 'B1', 'B2']
df1['r'] = dfx

with open('betti-sum-pbn-SOL_c3.txt','rb') as fp2:
    X2 = pickle.load(fp2)
df2 = pd.DataFrame(X2)/1001
df2.columns = ['B0', 'B1', 'B2']
df2['r'] = dfx

with open('betti-sum-pbn-water_c4.txt','rb') as fp3:
    X3 = pickle.load(fp3)
df3 = pd.DataFrame(X3)/1001
df3.columns = ['B0', 'B1', 'B2']
df3['r'] = dfx

with open('betti-sum-pbn-SOL_c5.txt','rb') as fp3:
    X3 = pickle.load(fp3)
df4 = pd.DataFrame(X3)/1001
df4.columns = ['B0', 'B1', 'B2']
df4['r'] = dfx


#plot with matplotlib
fig, axs = plt.subplots(nrows=1,ncols=3,figsize=(16,4))
#df.plot(x='r', y='B0',ax = axs[0], legend=True, color='red')
df1.plot(x='r', y='B0', ax = axs[0], legend=True, color='red')
df2.plot(x='r', y='B0', ax=axs[0], legend=True, color='blue',linestyle='solid')
df3.plot(x='r', y='B0', ax=axs[0], legend=True, color='green',linestyle='solid')
df4.plot(x='r', y='B0', ax=axs[0], legend=True, color='purple',linestyle='solid')

#df.plot(x='r', y='B1',ax = axs[1], legend=True, color='red')
df1.plot(x='r', y='B1', ax = axs[1], legend=True, color='red')
df2.plot(x='r', y='B1', ax=axs[1], legend=True, color='blue',linestyle='solid')
df3.plot(x='r', y='B1', ax=axs[1], legend=True, color='green',linestyle='solid')
df4.plot(x='r', y='B1', ax=axs[1], legend=True, color='purple',linestyle='solid')

#df.plot(x='r', y='B2',ax = axs[2], legend=True, color='red')
df1.plot(x='r', y='B2', ax = axs[2], legend=True, color='red')
df2.plot(x='r', y='B2', ax=axs[2], legend=True, color='blue',linestyle='solid')
df3.plot(x='r', y='B2', ax=axs[2], legend=True, color='green',linestyle='solid')
df4.plot(x='r', y='B2', ax=axs[2], legend=True, color='purple',linestyle='solid')



axs[0].legend(['B3','C3','C4','C5'])
axs[0].set_ylim((0,125))
axs[0].yaxis.set_ticks([20,40,60,80,100,120])
axs[0].set_ylabel('PBN0', fontsize='large', fontweight='bold')

axs[1].legend(['B3','C3','C4','C5'])
axs[1].set_ylim((0,20))
axs[1].yaxis.set_ticks([5,10,15,20])
axs[1].set_ylabel('PBN1', fontsize='large', fontweight='bold')

axs[2].legend(['B3','C3','C4','C5'])
axs[2].set_ylim((0,0.4))
axs[2].yaxis.set_ticks([0.1,0.2,0.3,0.4])
axs[2].set_ylabel('PBN2', fontsize='large', fontweight='bold')
#axs[0].text(0.1, 7, r'C1', fontsize='large', fontweight='bold')
#axs[1].text(0.1, 7, r'C2', fontsize='large', fontweight='bold')

for ax in axs.flat:
    #ax.set(xlabel='r(nm)', ylabel='g(r)')
    #ax.set_ylim((0,8))
    ax.set_xlim((0,1.9))
    #ax.yaxis.set_ticks([2, 4, 6, 8])
    ax.xaxis.set_ticks([0.5, 1, 1.5])
    ax.set_xlabel('r(nm)', fontsize='large', fontweight='bold')
    #ax1.xaxis.set_label_text("r(nm)")
    #ax.set_ylabel('PBE0', fontsize='large', fontweight='bold')
    #ax.set_title("Radial distribution function")
    #ax.legend(bbox_to_anchor=(0.5, 1))
    

# Hide x labels and tick labels for top plots and y ticks for right plots.
#for ax in axs.flat:
#    ax.label_outer()
fig.tight_layout()


fig.tight_layout()

plt.savefig('av-PBE-B3-C3-C4-C5-WATER.png', dpi=600)

